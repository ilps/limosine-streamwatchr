"""
Recommender class

author: Manos Tsagkias <e.tsagkias@uva.nl>
date: 21 November 2013
"""
from datetime import datetime, timedelta
from math import exp
import multiprocessing
# import and set logging
import logging
logging.basicConfig(level=logging.DEBUG, format='[%(levelname)s] (%(processName)s - %(threadName)-10s) %(message)s',)

class NPRecommenderWorker(multiprocessing.Process):
    def __init__(self, queue, dbnp, dbnp_rec):
        super(NPRecommenderWorker, self).__init__()
        
        self.queue = queue
        self.dbnp = dbnp
        self.dbnp_rec = dbnp_rec
        
        self.HALFLIFETIMEUNIT = 6*3600 # half the score every 6 hours
        

    def run(self):
        while True: 
            tweet = self.queue.get()
            if not tweet:
                break
            
            # Ignore tweets from Radios
            # if tweet["isRadio"]:
            #     continue
            
            obj = {"type": "song",
                   "target": {"song": tweet["song"].lower(), 
                              "artists": tweet["artists"]}}
            
            # logging.debug(tweet["userID"])
            now = datetime.utcnow()
            # logging.debug(now-timedelta(hours=12))
            # Get the last 12 (one CD) plays from a user in the last 12 hours
            for record in self.dbnp.archive.find({"tweet.user.id": tweet["userID"], 
                                                  "tweet.created_at": {"$gt": now-timedelta(hours=24)},
                                                  "song": {"$exists": True}}, 
                                                  {"tweet.created_at": True, 
                                                   "song": True,
                                                   "artists": True
                                                  }).hint([
                                                      ('tweet.user.id', 1), 
                                                      ('tweet.created_at', -1), 
                                                      ('song', 1)
                                                  ]).sort("tweet.created_at", -1)[:5]:
                # logging.debug(record)
                # assign a score depending on the timestamp
                origscore = exp(-0.69315 * (now - record["tweet"]["created_at"]).seconds / self.HALFLIFETIMEUNIT)
                
                obj["source"] = {"song": record["song"].lower(), 
                                 "artists": record["artists"]}
                
                if obj["source"] == obj["target"]:
                    continue
                    
                # get previous score
                score = origscore
                prevscore = self.dbnp_rec.graph.find_one(obj)
                if prevscore is not None:
                    score +=  prevscore["weight"] * exp(-0.69315 * (now - prevscore["seen"]).seconds / self.HALFLIFETIMEUNIT)
                self.dbnp_rec.graph.update(obj, 
                                        {"$set": {
                                            "seen": now, 
                                            "weight": score
                                        }}, upsert=True)
                
                # Create graph for artists only
                for target_artist in tweet["artists"]:
                    for source_artist in record["artists"]:
                        if target_artist == source_artist:
                            continue
                            
                        score = origscore
                        # get previous score
                        prevscore = self.dbnp_rec.graph.find_one({"type": "artist", "source": source_artist, "target": target_artist})
                        if prevscore is not None:
                            score += prevscore["weight"] * exp(-0.69315 * (now - prevscore["seen"]).seconds / self.HALFLIFETIMEUNIT)
                        self.dbnp_rec.graph.update({"target": target_artist, 
                                                "source": source_artist, 
                                                "type": "artist"}, 
                                                {"$set": {
                                                    "seen": now, 
                                                    "weight": score
                                                }}, upsert=True)
                        
                        
                        