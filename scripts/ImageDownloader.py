import requests
from config import NPKeys, NPDatabase
import os.path
from PIL import Image

if __name__ == "__main__":
	connection = NPDatabase()
	db = connection.get_db()

	sizes = {'small':'0','medium':'1','large':'2','extralarge':'3','mega':'4'}
	path = ""
	seen = 0;

	for record in db.artist_info.find(timeout=False):
		throwError = False

		seen += 1
		mbId = record['mbId']
		images = record['lastfm_info']['artist']['image']
		store_images = []

		if images[0]['#text'] == "":
			db.artist_info.update({"mbId":mbId}, {"$set": {"hasImages":False}}, upsert=True)
			continue

		for image in images:
			if image['#text'] == "":
				throwError = True
				break

			image_url = image['#text']
			extension = os.path.splitext(image_url)[1]
			filename = sizes[image['size']] + "/" + mbId + extension
			full_filename = path + filename

			try:
				r = requests.get(image_url, stream=True)
				if r.status_code == 200:
					with open(full_filename, 'wb') as f:
						for chunk in r.iter_content(1024):
							f.write(chunk)
			except:
				throwError = True
				break			

			try:
				im = Image.open(full_filename)
				(width,height) = im.size
			except:
				throwError = True
				break

			class_name = "ver"
			if width > height:
				class_name = "hor"

			store_images.append( {'url':filename, 'class':class_name} )

		if throwError:
			db.artist_info.update({"mbId":mbId}, {"$set": {"hasImages":False}}, upsert=True)
			continue

		db.artist_info.update({"mbId":mbId}, {"$set": {"hasImages":True, "local_images":store_images}}, upsert=True)

		if seen % 1000 == 0:
			print "Seen %d artists" % seen
