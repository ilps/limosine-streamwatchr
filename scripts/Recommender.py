"""
(We use a second capped collection which is the exact duplicate of currently_playing
because MongoDB consumes too much CPU on multiple tailable cursors
on the same collection. As of 25 Nov 2013 this is an open issue: 
https://jira.mongodb.org/browse/SERVER-9580)

Build recommendation graph for artists and songs

author: Manos Tsagkias <e.tsagkias@uva.nl>
date: 21 November 2013
"""
from config import NPDatabase
from NPRecommenderWorker import NPRecommenderWorker

import pymongo
import multiprocessing
# import and set logging
import logging
logging.basicConfig(level=logging.DEBUG, format='[%(levelname)s] (%(processName)s - %(threadName)-10s) %(message)s',)

if __name__ == "__main__":

	connection = pymongo.Connection("")
	dbnp = connection.npdemo
	dbnp.authenticate('','')

	dbnp_rec = connection.npdemo_recommender
	dbnp_rec.authenticate('','')

	dbnp_local = NPDatabase().get_db()

	queue = multiprocessing.Queue()

	# start processing queue
	workers = []
	for i in range(5):
		workers.append( NPRecommenderWorker(queue, dbnp_local, dbnp_rec) )
		workers[-1].daemon = True
		workers[-1].start()

	latest_id = dbnp.currently_playing_dup.find_one({}, sort = [('$natural',-1)])['_id']

	while True:
		cursor = dbnp.currently_playing_dup.find( {'_id' : {'$gt':latest_id} }, await_data=True, tailable=True ).sort([('$natural',1)])

		while True:
			tweet = next(cursor,None)

			if not tweet:
				if not cursor.alive:
					break
				continue

			queue.put(tweet)

			if queue.qsize() > 0 and queue.qsize() % 100 == 0:
				logging.info("Queue size: %d" % queue.qsize())

		latest_id = dbnp.currently_playing_dup.find_one().sort([('$natural',-1)])['_id']

	# For some reason we ended up here.
	# Shut down gracefully...
	for i in len(workers):
		queue.put(None)

	for worker in workers:
		worker.join()

	logging.info("QueueGenerator exited gracefully.")
    
    
