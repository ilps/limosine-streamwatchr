#!/usr/bin/python2.6

import MusicBrainz as mb
import TextProcessor as tp

class SongExtractor(object):

    def __init___(self):
    	pass
    	
    def extract(self, tweet):
        raise NotImplementedError( "Should have implemented this" )
