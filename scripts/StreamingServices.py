#!/usr/bin/python2.6
#encoding: utf-8

import json
import re
import requests

import logging
logging.getLogger("requests").setLevel(logging.WARNING)

import SongExtractor as se

class StreamingServices(se.SongExtractor):

    def __init__(self):
        super( se.SongExtractor, self ).__init__()

        self.patterns = { 
            'spotify' :
                {'hashtag':'spotify',
                'regex': re.compile(r'<title>(.+) by (.+?) on Spotify</title>', re.IGNORECASE | re.MULTILINE),
                'artist':2,
                'song':1} ,

            'itunes' : 
                {'name':'itunes',
                'hashtag':'itunes',
                'regex': re.compile(r'<title>iTunes - Music - (.+) by (.+?)</title>', re.IGNORECASE | re.MULTILINE),
                'artist':2,
                'song':1},

            'deezer' :
                {'name':'deezer',
                'hashtag':'deezer',
                'regex': re.compile(r'<title>(.+) - (.+?)</title>', re.IGNORECASE | re.MULTILINE),
                'artist':1,
                'song':2}
        }

        self.tags = []
        for t in self.patterns:
            self.tags.append( self.patterns[t]['hashtag'] )

    
    def get_tags(self):
        return self.tags

    def extract(self, tweet):
        service = None

        for tw_tag in tweet['entities']['hashtags']:
            if tw_tag['text'].lower() in self.patterns:
                service = self.patterns[tw_tag['text'].lower()]
                break

        if service is None:
            return None

        # first, extract all urls
        for url in tweet['entities']['urls']:
            html_response = self.get_url(url)
            if html_response:
                resolved_url = html_response['url']
                html_page = html_response['text']

                if re.search(r'(playlist/|album/|account/)',resolved_url, re.IGNORECASE):
                    continue

                matches = re.search(service['regex'], html_page)
                if matches:
                    artist = se.tp.translate_diacritics(se.tp.unescape(matches.group(service['artist'])))
                    artist = se.tp.remove_featuring( se.tp.replace_ampersand( artist )).strip()

                    song = se.tp.translate_diacritics(se.tp.unescape(matches.group(service['song'])))
                    song = se.tp.remove_featuring(se.tp.replace_ampersand( song )).strip()

                    check = se.mb.exact_match(artist, song, False)
                    if check is None:
                        check = se.mb.fuzzy_match(artist, song, False)
                        if check:
                            return check
                    else:
                        return check
        return None

    def get_url(self, url):
        resolved_url = None
        try:
            r = requests.get(url, timeout=1)
            resolved_url = r.url
        except:
            return None

        r.encoding = 'utf-8'
        return {'text':r.text, 'url':resolved_url}

