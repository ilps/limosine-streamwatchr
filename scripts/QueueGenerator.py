#encoding: utf-8

from NPExtractor import NPWorker
from NPRecommenderWorker import NPRecommenderWorker

from config import NPDatabase

import pymongo
import logging
import multiprocessing


if __name__ == "__main__":

	connection = pymongo.Connection("")
	dbnp = connection.npdemo
	dbnp.authenticate('','')

	dbnp_local = NPDatabase().get_db()

	queue = multiprocessing.Queue()
	
	# start processing queue
	workers = []
	for i in range(10):
		workers.append( NPWorker(queue, dbnp_local) )
		workers[-1].daemon = True
		workers[-1].start()
	
	latest_id = dbnp.crawler.find_one({}, sort = [('$natural',-1)])['_id']

	while True:
		cursor = dbnp.crawler.find( {'_id' : {'$gt':latest_id} }, await_data=True, tailable=True ).sort([('$natural',1)])

		while True:
			tweet = next(cursor,None)

			if not tweet:
				if not cursor.alive:
					break
				continue

			queue.put(tweet)
			
			if queue.qsize() > 0 and queue.qsize() % 100 == 0:
				logging.info("Queue size: %d" % queue.qsize())


		latest_id = dbnp.crawler.find_one().sort([('$natural',-1)])['_id']

	# For some reason we ended up here.
	# Shut down gracefully...
	for i in len(workers):
		queue.put(None)

	for worker in workers:
		worker.join()

	logging.info("QueueGenerator exited gracefully.")