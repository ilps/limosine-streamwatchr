import sys
import re
import logging
import collections
import operator

from heapq import heappush, nlargest
from time import sleep
from datetime import datetime, timedelta
from math import log, exp

import lxml.html

import ttp
import tweepy
import heapq

import pymongo as pm


logging.basicConfig(level=logging.DEBUG, format='[%(levelname)s] (%(threadName)-10s) %(message)s',)



""" Text cleaning regexes """
rs = re.compile(r"\s+", re.UNICODE | re.MULTILINE)
rnp = re.compile(r"\b(rt|via|retweet(ing)?)\b", re.UNICODE)
rp = re.compile(r"(\W+)", re.UNICODE)

timedamping = 0.693 # ln2
timeunit = 60

if __name__ == "__main__":
    # Connect to MongoDB and set up database
    logging.info("Connecting to MongoDB on YOURMACHINE")
    connection = pm.Connection("")
    dbnp = connection.npdemo
    dbnp.authenticate("", "") # readonly account: demo, npd3m0
    dbnp.summaries.ensure_index([("mbId", 1), ("song", 1)])
    dbnp.summaries.ensure_index("enddate", -1)
    logging.info("done")
    
    
    lastrunat = datetime.utcnow().replace(second=0, microsecond=0) - timedelta(seconds=61)
    while True:
        now = datetime.utcnow().replace(second=0, microsecond=0)
        prevminute = now - timedelta(seconds=60)
        
        timediff = datetime.utcnow() - lastrunat
        if timediff  <  timedelta(seconds=60):
            sleep(timediff.seconds)
            continue
        
        
        
        
        # Get most popular songs
        data = []
        for record in dbnp.song_popularity.find({"ts": {"$gt": now - timedelta(seconds=600)}}):
            heapq.heappush(data, (record["tempdf"] * exp(- timedamping * (now - record["ts"]).seconds / timeunit), record["song"]))
        
        # Get the artist/song with largest tempdf:
        largest = heapq.nlargest(1, data)[0]
        
        logging.info("Winner song is: %s (%.4f)" % (largest[1], largest[0]))
        # Check if it was trending before
        updateenddate = False
        for record in dbnp.summaries.find({"song": {"$exists": True}}).sort("enddate", -1)[:1]:
            if record["song"] == largest[1]:
                dbnp.summaries.update({"song": largest[1]}, {"$set": {"enddate": now}}, upsert=True)
                logging.info("   was also previous winner. Updated end_date.")
                updateenddate = True
        
        if not updateenddate:
            dbnp.summaries.update({"song": largest[1]}, {"$set": {"enddate": now, "startdate": now}}, upsert=True)
        
        
        # Get the most popular artist
        data = []
        for record in dbnp.artist_popularity.find({"ts": {"$gt": now - timedelta(seconds=120)}}):
            heapq.heappush(data, (record["tempdf"] * exp(- timedamping * (now - record["ts"]).seconds / timeunit), record["mbId"]))

        # Get the artist/song with largest tempdf:
        largest = heapq.nlargest(1, data)[0]

        logging.info("Winner artist is: %s (%.4f)" % (largest[1], largest[0]))
        # Check if it was trending before
        updateenddate = False
        for record in dbnp.summaries.find({"mbId": {"$exists": True}}).sort("enddate", -1)[:1]:
            if record["mbId"] == largest[1]:
                dbnp.summaries.update({"mbId": largest[1]}, {"$set": {"enddate": now}}, upsert=True)
                logging.info("   was also previous winner. Updated end_date.")
                updateenddate = True

        if not updateenddate:
            dbnp.summaries.update({"mbId": largest[1]}, {"$set": {"enddate": now, "startdate": now}}, upsert=True)
        
        
        # Update our clock. 
        lastrunat = now
        
