
import pymongo
from pg8000 import DBAPI
import random
import local_config

class NPKeys:

	def __init__(self):
		consumer_key = ""
		consumer_secret = ""
		access_key = ""
		access_secret = ""
		self.twitter_keys = {'consumer_key':consumer_key, 'consumer_secret':consumer_secret, 'access_key':access_key, 'access_secret':access_secret}

		l_consumer_key = ""
		l_consumer_secret = ""
		l_access_key = ""
		l_access_secret = ""
		self.lyrics_keys = {'consumer_key':l_consumer_key, 'consumer_secret':l_consumer_secret, 'access_key':l_access_key, 'access_secret':l_access_secret}

		self.youtube_keys = [
			''
		]

		self.lastfm_keys = [
			""
		]

		self.lyricsnmusic_keys = [
			""
		]

	def get_twitter_keys(self):
		return self.twitter_keys

	def get_twitter_lyrics_keys(self):
		return self.lyrics_keys

	def get_youtube_keys(self, is_random=False):
		if is_random:
			return random.choice(self.youtube_keys)

		return self.youtube_keys

	def get_lastfm_keys(self, is_random=False):
		if is_random:
			return random.choice(self.lastfm_keys)

		return self.lastfm_keys

	def get_lyricsnmusic_keys(self, is_random=False):
		if is_random:
			return random.choice(self.lyricsnmusic_keys)

		return self.lyricsnmusic_keys



class NPDatabase:

	def __init__(self):
		self.connection = pymongo.Connection(local_config.mongo_server, local_config.mongo_port)
		self.dbnp = self.connection[local_config.mongo_db]
		self.dbnp.authenticate(local_config.mongo_user, local_config.mongo_pw)

	def get_db(self):
		return self.dbnp


class NPGeoDB:

	def __init__(self):
		self.pgcon = DBAPI.connect(host=local_config.pg_host, user=local_config.pg_user, database=local_config.pg_db)

	def get_db(self):
		return self.pgcon


class NPTracker:

	def __init__(self):
		self.keywords = [
			'np',
		]

	def get_hashtags(self):
		hashtags = []
		for k in self.keywords:
			hashtags.append("#%s" % k)
		return hashtags

	def get_keywords(self):
		return self.keywords


