#!/usr/bin/python2.6
#encoding: utf-8

# import config file
from config import NPKeys, NPGeoDB

import sys
import json
import re
import math
import datetime
import time
import operator
from collections import defaultdict
import requests
import multiprocessing

# import and set logging
import logging
logging.basicConfig(level=logging.DEBUG, format='[%(levelname)s] (%(processName)s - %(threadName)-10s) %(message)s',)
logging.getLogger("requests").setLevel(logging.WARNING)

# import home-made extractors
from StreamingServices import StreamingServices
from YoutubeResults import YoutubeResults
from BaselineRegex import BaselineRegex

# import geo stuff
from Point import Point


class NPWorker(multiprocessing.Process):

    def __init__(self, queue, dbnp):
        super(NPWorker, self).__init__()
        
        self.queue = queue
        self.dbnp = dbnp

        # get Geo DB
        self.pgcon = NPGeoDB().get_db()

        # instantiate keys
        self.keys = NPKeys()
        
        # Age discounting knobs
        # Half df for artist/song pair every 10mins without a tweet
        self.timedamping = 0.693 # ln2
        self.timeunit = 10*60

        # instantiate extraction methods
        self.streamservices = StreamingServices()
        self.youtuberesults = YoutubeResults()
        self.baselineregex = BaselineRegex()
        
    
    def run(self):
        while True:
            tweet = self.queue.get()
            if not tweet:
                break

            self.dbnp.archive.update({'tweetid': tweet['id']}, {"$set": {'tweet':tweet}}, upsert=True)

            # add tweet to queue
            # ignore retweets, spam, and replies
            if self.is_retweet(tweet) or self.is_spam(tweet) or self.is_reply(tweet) or self.stuck_in_filter(tweet):
                self.dbnp.archive.update({'tweetid': tweet['id']}, {"$set": {'method':'IGNORED'}})
                self.dbnp.methods.update( {'method':'IGNORED'}, {"$inc":{'count':1}}, upsert=True)
                continue

            # check if user is a radio
            isRadio = self.is_radio(tweet)

            # start extraction
            extracted = None
            method = None

            # check if tweet contains streaming service hashtags
            found_tag = False
            for tag in tweet['entities']['hashtags']:
                if tag['text'].lower() in self.streamservices.get_tags():
                    found_tag = True
                    break

            if found_tag:
                extracted = self.streamservices.extract(tweet)
                method = "streaming_service"
            else:
                # try to get song info using baseline regexes
                extracted = self.baselineregex.extract(tweet)
                method = "baseline"
                if extracted is None:
                    # move to regexes on Youtube results
                    extracted = self.youtuberesults.extract(tweet)
                    method = "youtube"

            # if we have a result, store it
            if extracted:
                self.dbnp.methods.update( {'method':method}, {"$inc":{'count':1}}, upsert=True)
                self.dbnp.archive.update({'tweetid': tweet['id']}, {"$set": {'method':method, 'artists':extracted['artists'], 'song':extracted['song'], 'isRadio':isRadio }})
                self.store(extracted['artists'], extracted['song'], isRadio, tweet)
            else:
                self.dbnp.archive.update({'tweetid': tweet['id']}, {"$set": {'method':'NO_MATCH' }})
                self.dbnp.methods.update( {'method':'NO_MATCH'}, {"$inc":{'count':1}}, upsert=True)



    def store(self, artists, song, isRadio, tweet):
        song = song.strip().lower()
        artist_list = []
        artist_ids = []

        # truncate timestamp to hours (for statistics)
        timestamp = tweet['created_at']
        dates = [
                    ['hourly', timestamp.replace(minute=0,second=0,microsecond=0)],
                    ['daily', timestamp.replace(hour=0,minute=0,second=0,microsecond=0)],
                    ['monthly', timestamp.replace(day=1,hour=0,minute=0,second=0,microsecond=0)],
                    ['yearly', timestamp.replace(month=1,day=1,hour=0,minute=0,second=0,microsecond=0)]
                ]

        # try to get geo info
        geoinfo = None
        if tweet['geo']:
            # if tweet geo info: get location using coordinates
            geoinfo = self.get_location_info(tweet['geo'])
        elif tweet['user']['location']:
            # else: try to match user provided location string
            geoinfo = self.get_location_info(tweet['user']['location'],False)

        # go over all extracted artists
        for artist in artists:
            mbId = artist["mbId"]
            artist = artist["name"]
            artist_list.append(artist)
            artist_ids.append(mbId)

            # Update artist popularity using Manos DF
            tempdf = 1
            record = self.dbnp.artist_popularity.find({"mbId": mbId}).hint([('mbId',1)])
            if record.count() > 0:
                ts = record[0]["ts"]
                factor =  math.exp(- self.timedamping * (timestamp - ts).seconds / self.timeunit)
                tempdf = record[0]["tempdf"] * factor + 1
                #tempdf = record["tempdf"] + 1

            # Get artist info from Last.fm
            mbId_info = self.dbnp.artist_info.find({"mbId": mbId}).hint([('mbId',1)])
            if mbId_info.count() == 0:
                artist_info = self.get_lastfm_artist_info(mbId)
                if artist_info is not None:
                    try:
                        artist_json = artist_info.json()
                    except ValueError, e:
                        logging.warn("Artist JSON cannot be decoded. Skipping update.")
                    else:
                        if artist_json is not None and "artist" in artist_json:
                            self.dbnp.artist_info.update({"mbId":mbId}, {"$set": {"lastfm_info":artist_json}}, upsert=True)

            self.dbnp.artist_popularity.update({"mbId": mbId}, {"$set": {"ts": timestamp, "tempdf": tempdf, "artist": artist}}, upsert=True)
            
            # Update artist popularity using Random Matrix (Periscope)
            self.store_random_matrix("artist", mbId, timestamp)
            
            # Store artist overall statistics
            # per hour, day, month, and year
            for d in dates:
                collection = 'artist_stats_overall_'+d[0]
                self.dbnp[collection].update({"mbId":mbId,'isRadio':isRadio,'date':d[1]},{"$inc":{'count':1}}, upsert=True)

                if geoinfo:
                    collection_geo = 'artist_stats_geo_'+d[0]
                    self.dbnp[collection_geo].update({"mbId":mbId,'country':geoinfo['country_iso'],'isRadio':isRadio,'date':d[1]},{"$inc":{'count':1},"$set":{'continent':geoinfo['continent'],'population':geoinfo['population']}}, upsert=True)

            self.dbnp.artist_stats_overall.update({"mbId":mbId,'isRadio':isRadio},{"$inc":{'count':1}}, upsert=True)
            if geoinfo:
                self.dbnp.artist_stats_geo.update({"mbId":mbId,'country':geoinfo['country_iso'],'isRadio':isRadio},{"$inc":{'count':1},"$set":{'continent':geoinfo['continent'],'population':geoinfo['population']}}, upsert=True)

        # Update song popularity using Manos DF
        tempdf = 1
        record = self.dbnp.song_popularity.find({"artistMbId": ", ".join(artist_ids), "song":song}).hint([('artistMbId',1),('song',1)])
        if record.count() > 0:
            ts = record[0]["ts"]
            factor =  math.exp(- self.timedamping * (timestamp - ts).seconds / self.timeunit)
            tempdf = record[0]["tempdf"] * factor + 1
        self.dbnp.song_popularity.update({"artistMbId": ", ".join(artist_ids), "song":song}, {"$set": {"ts": timestamp, "tempdf": tempdf, "artists": artists}}, upsert=True)
        
        # Update song popularity using Random Matrix (Periscope)
        self.store_random_matrix("song", song, timestamp)

        # Store overall song statistics
        # per hour, day, month, and year
        for d in dates:
            collection = 'song_stats_overall_'+d[0]
            self.dbnp[collection].update({"artists":artists, "song":song, 'isRadio':isRadio, "date":d[1]},{"$inc":{'count':1}}, upsert=True)
            self.dbnp[collection].update({"artists":"all", 'isRadio':isRadio, "date":d[1]},{"$inc":{'count':1}}, upsert=True)

            if geoinfo:
                self.dbnp['song_stats_geo_'+d[0]].update({"artists":artists, "song":song,"country":geoinfo['country_iso'],'isRadio':isRadio,"date":d[1]},{"$inc":{'count':1},"$set":{'continent':geoinfo['continent'],'population':geoinfo['population']}}, upsert=True)
                self.dbnp['country_stats_'+d[0]].update({"country":geoinfo['country_iso'],'isRadio':isRadio,"date":d[1]},{"$inc":{'count':1}}, upsert=True)

        self.dbnp.song_stats_overall.update({"artists":artists, 'isRadio':isRadio, "song":song},{"$inc":{'count':1}}, upsert=True)
        self.dbnp.song_stats_overall.update({"artists":"all", 'isRadio':isRadio},{"$inc":{'count':1}}, upsert=True)

        if geoinfo:
            self.dbnp.song_stats_geo.update({"artists":artists, "song":song, "country":geoinfo['country_iso'],'isRadio':isRadio},{"$inc":{'count':1},"$set":{'continent':geoinfo['continent'],'population':geoinfo['population']}}, upsert=True)

        # Store current tweet in capped collection
        artist_string = ", ".join(artist_list)
        p = None
        mbId_info = self.dbnp.artist_info.find({"mbId": artists[0]["mbId"]})
        if mbId_info.count() > 0:
            p = mbId_info[0]["lastfm_info"]["artist"]["image"]
        #self.dbnp.currently_playing.insert({"mbId":artists, "artist":artist_string, "artists":artists, "song": song, "imageUrl":p, "user":tweet['user']['screen_name'], "isRadio":isRadio, "userID" : tweet['user']['id'], "tweet_id": tweet['id']})

        # Store Youtube videoId
        video_info = self.dbnp.song_info.find({"artistMbId": ", ".join(artist_ids), "song":song}).hint([('artistMbId',1),('song',1)])
        videoId = None
        if video_info.count() == 0:
            query = '"' + artists[0]['name'] + '" "' + song + '"'
            videoId = self.get_youtube_video(query)
            if videoId:
                self.dbnp.song_info.insert({"artists": artists, "song":song, "videoId":videoId, "artistMbId": ", ".join(artist_ids), "date":datetime.datetime.utcnow(), "update_count":0})
        else:
            videoId = video_info[0]['videoId']

        self.dbnp.currently_playing.insert({"mbId":artists, "artist":artist_string, "artists":artists, "song": song, "images":p, "user":tweet['user']['screen_name'], "isRadio":isRadio, "userID" : tweet['user']['id'], "tweet_id": tweet['id'], "video_id": videoId})
        self.dbnp.currently_playing_dup.insert({"mbId":artists, "artist":artist_string, "artists":artists, "song": song, "images":p, "user":tweet['user']['screen_name'], "isRadio":isRadio, "userID" : tweet['user']['id'], "tweet_id": tweet['id'], "video_id": videoId})
    #end of store()
    
    
    
    def store_random_matrix(self, entitytype, entity, lastseen):
        """ Computes the Random Matrix statistics (aka Periscope)
        on artists (mbId), and songs. Entitytype should be either "song", 
        or "artist", and entity should be the song or the artist
        name/id.
        """
        rec = self.dbnp.randommatrix.find({"type": entitytype, "entity": entity}).hint([('type',1),('entity',1)])
        if rec.count() == 0:
            rec = {"lastpos": 0, "lastmean": 0., "laststd": 0., "lastsum": 0}
        else:
            rec = rec[0]
        
        if rec["lastmean"]:
            x_i = (lastseen - rec["lastpos"]).total_seconds() / rec["lastmean"]
        else:
            x_i = 1
        prevstd = rec["laststd"]
        rec["laststd"] = rec["laststd"]**2 * rec["lastsum"]
        rec["lastsum"] += 1
        m = rec["lastmean"] + (x_i - rec["lastmean"]) / rec["lastsum"]
        s = rec["laststd"] + (x_i - m) * (x_i - rec["lastmean"])
        
        if rec["lastpos"] and lastseen <= rec["lastpos"]:
            logging.error("Size is SMALLER, or diff is ZERO!")
            logging.error("QSIZE: %d, entitytype: %s, entity: %s, s: %f, rec['lastsum']: %d, prevstd: %f, size: %s, rec['lastpos']: %s, diff: %d" % (
                            self.queue.qsize(), 
                            entitytype,
                            entity, 
                            math.sqrt(s/rec["lastsum"]), 
                            rec["lastsum"], 
                            prevstd, 
                            lastseen, 
                            rec["lastpos"], 
                            (lastseen - rec["lastpos"]).total_seconds()))
            logging.error("skipping")
            return
        
        stdrate = 0
        if rec["lastpos"]:
            stdrate = (math.sqrt(s/rec["lastsum"]) - prevstd) / (lastseen - rec["lastpos"]).total_seconds()
        self.dbnp.randommatrix.update({"type": entitytype, "entity": entity}, 
                                {"$set":{
                                    "lastseen": lastseen.replace(second=0, microsecond=0),
                                    "lastpos": lastseen,
                                    "lastmean": m,
                                    "laststd": math.sqrt(s/rec["lastsum"]),
                                    "stdrate": stdrate},
                                    "$inc": {"lastsum": 1}
                                }, upsert=True)
    
    def get_lastfm_artist_info(self, mbId):
        key = self.keys.get_lastfm_keys(is_random=True)

        # parameters for last.fm
        params  = {'method':"artist.getinfo", 'mbid':mbId, 'api_key':key, 'format':"json", 'autocorrect':1}
        try:
            apiRequest = requests.get("http://ws.audioscrobbler.com/2.0/", params=params, timeout=1)
        except:
            return None

        return apiRequest


    def get_location_info(self, geo, coordinates=True):
        # logging.debug(geo)

        params = ()
        if 'coordinates' in geo:
            # Try to extract coordinates from structured data
            params = tuple(geo["coordinates"])
        else:
            # Parse a possible list of coordinates from a string
            try:
                geoobj = Point(re.sub(r"\n", " ", geo))
            except ValueError, e:
                pass
                # logging.error("Point cannot be parsed: %s" % e)
            else:
                params = (geoobj.latitude, geoobj.longitude)

        if not params:
            # Try to translate the text to a place
            if not geo.lower().strip():
                return None
            geo_lower =  [geo.lower().strip()]
            geo_lower.extend(map(unicode.strip, re.split(r",", geo.lower(), re.UNICODE)))
            geo_lower.extend(map(unicode.strip, re.split(r"-", geo.lower(), re.UNICODE)))
            geo_lower.extend(map(unicode.strip, re.split(r"/", geo.lower(), re.UNICODE)))
            geo_lower.extend(map(unicode.strip, re.findall(r"([-\w]{2,})", geo.lower(), re.UNICODE)))
            geo_lower = list(set(geo_lower))
            # logging.debug(geo_lower)
            
            params = (geo_lower, )
        
        if len(params) == 2: # latitude, longitude
            query = """SELECT
                   geoname.country 
                    , countryinfo.country
                    , countryinfo.continent 
                    , countryinfo.population
                    FROM geoname 
                    JOIN countryinfo ON geoname.country = countryinfo.iso_alpha2
                    WHERE earth_box(ll_to_earth(%s, %s), 50000) @> ll_to_earth(latitude, longitude) 
                    LIMIT 1"""
        else: # one array of many terms
            query = """SELECT
                            geoname.country
                            , countryinfo.country
                            , countryinfo.continent
                            , countryinfo.population
                        FROM geoname
                        JOIN countryinfo ON geoname.country = countryinfo.iso_alpha2
                        WHERE id = (
                            SELECT g.id
                            FROM alternatename a
                            JOIN geoname g ON g.id=a.geoname_id
                            WHERE a.alternate_name_lower = ANY (%s)
                            GROUP BY g.id, g.population
                            ORDER BY count(g.id) DESC, g.population DESC
                            LIMIT 1) """
                            
                            
        
        geoobj = None
        pgcur = self.pgcon.cursor()
        pgcur.execute( query, params )

        if pgcur.rowcount > 0:
            isocode, country, continent, population = pgcur.fetchone()
            geoobj = {"country_iso": isocode, "country_name": country, "continent": continent, "population":population}

        # logging.debug(geoobj)
        return geoobj

    def get_youtube_video(self,query):
        key = self.keys.get_youtube_keys(is_random=True)

        params  = {'part':"id", 'key':key, 'alt':'json', 'maxResults':1, 'type':'video', 'videoCategoryId':10, 'videoEmbeddable':'true', 'q':query.encode("utf-8")}
        try:
            search_response = requests.get("https://www.googleapis.com/youtube/v3/search", params=params, timeout=1)
            search_response.encoding = 'utf-8'
            json_object = search_response.json()

            if json_object["pageInfo"]["totalResults"] > 0:
               return json_object["items"][0]["id"]["videoId"]

            return None
        except:
            return None

        return None


    def is_spam( self, tweet ):
        if len(tweet['entities']['hashtags']) > 2:
            if len(tweet['entities']['urls']) > 0:
                logging.debug("IGNORED (spam): %s" % tweet['text'] )
                return True

        spam_users = [
        	"reiaran"
        ]

        for user in spam_users:
        	if tweet['user']['screen_name'] and user in tweet['user']['screen_name'].lower():
        		return True

        return False


    def is_retweet( self, tweet ):
        if tweet['retweeted'] == True or re.search(r'RT',tweet['text']):
            logging.debug("IGNORED (retweet): %s" % tweet['text'] )
            return True

        return False


    def is_reply( self, tweet ):
        if tweet['in_reply_to_status_id'] != None or re.search(r'@\w+?:', tweet['text']):
            logging.debug("IGNORED (reply): %s" % tweet['text'] )
            return True

        return False


    def is_radio( self, tweet ):
        radio_words = [
            "radio",
            "FM",
            "play"
        ]

        for word in radio_words:
            if (tweet['user']['screen_name'] and word in tweet['user']['screen_name'].lower()) or (tweet['user']['description'] and word in tweet['user']['description'].lower()):
                return True

        return False

    def stuck_in_filter( self, tweet ):
    	filter_terms = [
    		"@absoluteradio"
    	]

    	for term in filter_terms:
    		if term in tweet['text'].lower():
    			return True

    	return False
