#encoding: utf-8

from datetime import datetime, timedelta
from config import NPDatabase
import logging
import time

import json


COUNT_THRESHOLD = 35

last_message_timestamp = datetime.now()
logging.basicConfig(level=logging.DEBUG, format='[%(levelname)s] (%(threadName)-10s) %(message)s',)

if __name__ == "__main__":

    dbnp = NPDatabase().get_db()
    latest = datetime.utcnow().replace(minute=0, second=0, microsecond=0)

    d_hour = timedelta(seconds=3600)
    d_week = timedelta(days=14)

    hour_before = latest - d_hour
    week_before = latest - d_week

    added_discoveries = 0
    
    logging.info("Find newly discovered songs")
    candidate_set = dbnp.song_stats_overall_hourly.find({"date": {"$gte": hour_before, "$lt": latest}, "count": {"$gt": COUNT_THRESHOLD}, "isRadio":False}).hint([('date',-1),('count',1),('isRadio',1)])
    logging.info("  got %d candidates" % candidate_set.count())
    
    for candidate in candidate_set:
        if candidate['artists'] == "all":
            continue

        logging.debug("Candidate: '%s' by %s" % (candidate["song"], candidate["artists"][0]["name"]))
        previously_played = dbnp.song_stats_overall_daily.find({"artists":candidate['artists'], "song":candidate['song'], "date": {"$lt":week_before}}).hint([('artists',1),('song',1),('date',-1)])
        logging.debug("  has been played before")

        if previously_played.count() == 0:
            already_discovered = dbnp.discovered.find({"artists":candidate['artists'], "song":candidate['song']}).hint([('artists',1),('song',1),('date',-1)])
            logging.debug("  has been discovered before")

            if already_discovered == 0:
                    logging.debug("Found new discovery: "+ candidate['song'])
                    dbnp.discovered.insert({"artists":candidate['artists'], "song":candidate['song'], "date":latest})
                    added_discoveries += 1

    logging.info("Added %d discoveries. Exiting." % added_discoveries)

#end of if__main__
