import requests
from config import NPKeys, NPDatabase
import re

def query_lyrics_api(key,text):
    url = "http://api.lyricsnmusic.com/songs"
    params = {"api_key":key, "lyrics":text.encode("utf-8")}

    rjson = None
    try:
        r = requests.get(url, params=params, timeout=1)
        r.encoding = 'utf-8'
        return r.json()
    except:
        return None

    return None


if __name__ == "__main__":
    connection = NPDatabase()
    dbnp = connection.get_db()
    keys = NPKeys()

    for lyric in dbnp.lyrics.find(timeout=False):
        key = keys.get_lyricsnmusic_keys(is_random=True)
        text = lyric['tweet']

        text = re.sub(r'https?:\/\/.*[\r\n]*', '', text)
        text = re.sub(r'(#|@).+?(\s|$)', '', text)
        text = re.sub(r'[^a-zA-Z0-9_ ]', r' ', text)
        text = re.sub(r' {2,}', r' ', text)
        text = text.strip()

        lyrics = query_lyrics_api(key,text)

        if lyrics is not None:
            found_lyric = False
            artist = lyric['artists'][0]['name']

            for s in lyrics:
                if s['title'].lower() == lyric['song'].lower() and s['artist']['name'].lower() == artist.lower():
                    found_lyric = True
                    try:
                        temp_context = re.sub(r'</*em>', '', s['context'])
                    except:
                        found_lyric = False
                        break
                    temp_url = s['url']
                    break

            if found_lyric:
                print "Found lyric"
                print "TWEET: %s" % (lyric['tweet'])
                print
                print "LYRICS: %s" % (temp_context)
                print
                print "ARTIST: %s - %s" % (artist,lyric['song'])
                print
                print
                
                dbnp.lyrics_new.insert({'tweetid':lyric['tweetid'], 'tweet':lyric['tweet'], 'created_at': lyric['created_at'], 'artists':lyric['artists'], 'song':lyric['song'], 'lyrics':temp_context, 'url':temp_url, 'user' : lyric['user']})
                dbnp.lyrics_popularity_new.update({'artists':lyric['artists'], 'song':lyric['song']}, {'$inc': {'count':1} }, upsert=True)
                dbnp.lyrics_fragments_new.update({'artists':lyric['artists'], 'song':lyric['song'], 'lyrics':temp_context}, {'$inc':{'count':1}}, upsert=True)