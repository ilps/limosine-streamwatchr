from config import NPDatabase
import logging


if __name__ == "__main__":
    dbnp = NPDatabase().get_db()


    logging.info("Ensuring indices on collections")

    # methods
    dbnp.methods.ensure_index([("method",1)])

    # artist_info
    dbnp.artist_info.ensure_index([("mbId",1)])

    #artist_popularity
    dbnp.artist_popularity.ensure_index([("mbId",1)])
    dbnp.artist_popularity.ensure_index([("ts",-1), ("tempdf",-1)])

    #artist_stats_overall_*

    times = ['hourly','daily','monthly','yearly']

    for table in times:
        table_name = "_".join(['artist_stats_overall',table])
        dbnp[table_name].ensure_index([("mbId", 1), ("isRadio", 1), ("date", -1)])

        table_name = "_".join(['artist_stats_geo',table])
        dbnp[table_name].ensure_index([("mbId", 1), ("country", 1), ("isRadio", 1), ("date", -1)])

        table_name = "_".join(['song_stats_overall',table])
        dbnp[table_name].ensure_index([("artists", 1), ("song", 1), ("isRadio", 1), ("date", -1)])
        dbnp[table_name].ensure_index([("artists", 1), ("isRadio", 1), ("date", -1)])

        table_name = "_".join(['song_stats_geo',table])
        dbnp[table_name].ensure_index([("artists", 1), ("song", 1), ("country", 1), ("isRadio", 1), ("date", -1)])
        dbnp[table_name].ensure_index([("country", 1), ("date", -1), ("isRadio", 1), ("count", -1)])

        table_name = "_".join(['country_stats',table])
        dbnp[table_name].ensure_index([("country", 1), ("isRadio", 1), ("date", -1)])
        dbnp[table_name].ensure_index([("date", -1), ("isRadio", 1)])

    #Artist_stats_overall
    dbnp.artist_stats_overall.ensure_index([('mbId', 1),('isRadio', 1)])

    #Artist_stats_geo
    dbnp.atist_stats_geo.ensure_index([('mbId', 1), ('country', 1), ('isRadio', 1)])

    #Song_popularity
    dbnp.song_popularity.ensure_index([('artistMbId', 1), ('song', 1)])
    dbnp.song_popularity.ensure_index([('song', 1), ('tempdf', -1)])
    dbnp.song_popularity.ensure_index([('ts', -1), ('tempdf', -1)])

    #Song_stats_overall_hourly
    dbnp.song_stats_overall_hourly.ensure_index([('date', -1), ('count', 1), ('isRadio', 1)])

    #Song_stats_overall_daily
    dbnp.song_stats_overall_daily.ensure_index([('artists', 1), ('song', 1), ('date', -1)])

    #Song_stats_overall
    dbnp.song_stats_overall.ensure_index([('artists', 1), ('isRadio', 1), ('song', 1)])

    #Song_stats_geo
    dbnp.song_stats_geo.ensure_index([('artists', 1), ('song', 1), ('country', 1), ('isRadio', 1)])

    #Song_info
    dbnp.song_info.ensure_index([('artistMbId', 1), ('song', 1)])
    dbnp.song_info.ensure_index([('artists', 1), ('song', 1)])
    dbnp.song_info.ensure_index([('date', 1), ('update_count', 1)])

    #Archive
    dbnp.archive.ensure_index([('tweetid', 1)])
    dbnp.archive.ensure_index([('song', 1)])

    #Randommatrix
    dbnp.randommatrix.ensure_index([('type', 1), ('entity', 1)])
    dbnp.randommatrix.ensure_index([('type', 1), ('lastsum', 1), ('lastseen', -1), ('stdrate', 1), ('laststd', -1)])

    #Discovered
    dbnp.discovered.ensure_index([('artists', 1), ('song', 1), ('date', -1)])
    dbnp.discovered.ensure_index([('date', -1)])

    logging.info("...ensured")

    logging.info("Check if capped collection exists (needs to be created beforehand)")

    if not "currently_playing" in dbnp.collection_names():
        dbnp.create_collection('currently_playing', capped=True, size=1000000)
    else:
        options = dbnp.currently_playing.options()
        if 'capped' not in options or not options['capped']:
            dbnp.drop_collection('currently_playing')
            dbnp.create_collection('currently_playing', capped=True, size=1000000)
