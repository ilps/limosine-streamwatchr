#!/usr/bin/python2.6
#encoding: utf-8

import re
import HTMLParser
# import logging
# logging.basicConfig(level=logging.INFO, format='[%(levelname)s] (%(threadName)-10s) %(message)s',)

h = HTMLParser.HTMLParser()

def translate_diacritics(text):
  # Remove diacritics
  table = {
    0xe4: u'ae',
    ord(u'ß'): u'ss',
    ord(u'ë'): u'e',
    ord(u'é'): u'e',
    ord(u'è'): u'e',
    ord(u'ê'): u'e',
    ord(u'ö'): u'o',
    ord(u'ó'): u'o',
    ord(u'ò'): u'o',
    ord(u'ô'): u'o',
    ord(u'î'): u'i',
    ord(u'í'): u'i',
    ord(u'ì'): u'i',
    ord(u'ï'): u'i',
    ord(u'ú'): u'u',
    ord(u'ù'): u'u',
    ord(u'ü'): u'u',
    ord(u'û'): u'u',
    ord(u'á'): u'a',
    ord(u'à'): u'a',
    ord(u'ä'): u'a',
    ord(u'â'): u'a',
    ord(u'ÿ'): u'y',
    ord(u'å'): u'a',
    ord(u'œ'): u'oe',
    ord(u'ø'): u'o',
    ord(u'ç'): u'c',
    ord(u'ñ'): u'n',
    ord(u'æ'): u'ae'
  }
  return text.translate(table)

def dash_names_encode(title):
	# Keep dashes in artist names like Jay-Z, G-Dragon, T-ara, and Ne-Yo
	title = re.sub(r'\b([a-zA-Z]{1,2})-([A-Za-z]+?)\b', r'\1 DASH \2', title)
	title = re.sub(r'\b([A-Za-z].+?)-([a-zA-Z])\b', r'\1 DASH \2', title)
	return title.strip()

def dash_names_decode(title):
	title = title.strip()
	# Turn temporary artist names like "Jay dash Z" back into "Jay-Z"
	title = re.sub(r'\b([a-zA-Z]+?) dash ([A-Za-z]+?)\b', r'\1-\2', title)
	return title.strip()

def remove_extensions(title):
	# Remove extensions at the end (e.g. .wmv) and M/V (Asian songs)
	title = re.sub(r'\.[a-zA-Z]{3}$', r'', title)
	title = re.sub(r'm/v', r' ', title, re.IGNORECASE)
	return title.strip()

def remove_brackets(title):
	# Remove stuff between brackets (), [], or {}
	title = re.sub(r'\(.+?\)', r' ', title)
	title = re.sub(r'\[.+?\]', r' ', title)
	title = re.sub(r'\{.+?\}', r' ', title)
	title = re.sub(r'【.+?】', r' ', title)

	return title.strip()

def remove_end_brackets(title):
	# Remove stuff between brackets (), [], or {} at the end
	title = re.sub(r'\(.+?\)$', r'', title.strip())
	title = re.sub(r'\[.+?\]$', r'', title)
	title = re.sub(r'\{.+?\}$', r'', title)
	title = re.sub(r'【.+?】', r' ', title)

	return title.strip()

def replace_underscores(title):
	# Replace underscore by whitespace
	title = re.sub(r'_', r' ', title)
	return title.strip()

def remove_lyrics(title):
	# Remove lyrics text (e.g. "with lyrics")
	title = re.sub(r'(w |with |w\/ |w\. )*lyrics', r' ', title, re.IGNORECASE)
	return title.strip()

def remove_featuring(title):
	# Remove featuring text (e.g. "ft. Artist")
	title = re.sub(r' (ft\.|feat\.|ft|feat|featuring).+?-', r'-', title)
	title = re.sub(r' (ft\.|feat\.|ft|feat|featuring).+', r' ', title)
	return title.strip()

def replace_ampersand(title):
	# Replace ampersand by "and" 
	title = re.sub(r'&amp;', r' and ', title)
	title = re.sub(r'&', r' and ', title)
	return title.strip()

def char_by_char(t):
	punctuation = "[](){}^*+.?'/|~\"&@:;#!"
	# logging.debug("inside char_by_char %s" % t)
	if not t or t in punctuation:
		return ""
	
	# Remove preceding puncuation
	idx = 0
	while idx < len(t):
		if t[idx] in punctuation:
			idx +=1
		else:
			break
	t = t[idx:]
	# Remove trailing punctuation
	idx = 0
	while idx < len(t):
		if t[len(t)-1-idx] in punctuation:
			idx +=1
		else:
			break
	t = t[:len(t)-idx]
	return t


# def remove_punctuation_and_whitespaces(title):
# 	# Remove all punctuation
# 	title = re.sub(r'[\[\]\(\)\{\}\^\*\+\.\?\'/\|~"&@:;\\#]', r'', title)
# 	title = title.lower().strip()
# 	
# 	# Remove whitespaces and make lowercase
# 	title = re.sub(r'\s+', r' ', title)
# 	title = re.sub(r'^\s+', r'', title)
# 	title = re.sub(r'\s+$', r'', title)
# 	
# 	return title

# THIS IS MANO'S ATTEMPT, IT IS BUGGY: RANDOMLY SPITS OUT LISTS, 
# WHICH MAKES YOUTUBERESULTS TO CHOKE.
def remove_punctuation_and_whitespaces(title):
	# logging.debug("input term %s" % title)
	# Remove whitespaces and make lowercase
	title = re.sub(r'\s+', r' ', title)
	title = re.sub(r'^\s+', r'', title)
	title = re.sub(r'\s+$', r'', title)
	
	# logging.debug("After splitting %s" % re.split(r"\s+", title))
	
	# Check if there any words left
	words = re.split(r"\s+", title)
	if not words:
		words = [title] # add the whole title as a word
	# Iterate over the words
	cleaned = []
	for word in words:
		word = char_by_char(word)
		if not word:
			continue
		cleaned.append(word)
	
	title = u""
	if cleaned:
		title = " ".join(cleaned).lower().strip()
	# Remove all punctuation
	# title = re.sub(r'[\[\]\(\)\{\}\^\*\+\.\?\'/\|~"&@:;\\#]', r'', title)
	# title = title.lower().strip()
	# title = " ".join(cleaned).lower().strip()
	# logging.debug("before return %s" % title)
	return title

def remove_urls(text):
	# Remove al URLs from text
	text = text.lower()
	text = re.sub(r'http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', '', text)
	#text = re.sub(r'https?:\/\/.*[\r\n\s]*', '', text)
	return text.strip()

def remove_dash(title):
	# Remove everything after dash
	title = re.sub(r'\s-(\s.*|$)', r' ', title)
	return title.strip()

def remove_hashwords(title):
	title = re.sub(r"#(listenlive|radio|music|swtchr)", r' ', title)
	return title.strip()

def unescape(text):
	return h.unescape(text).strip()


def process_text(title):
	title = remove_extensions(title)
	title = remove_brackets(title)
	title = replace_underscores(title)
	title = remove_lyrics(title)
	title = remove_featuring(title)
	title = replace_ampersand(title)
	title = keep_dash_names(title)
	title = remove_punctuation_and_whitespaces(title)

	return title.strip()

def process_text_baseline(title):
	title = post_process(title)
	title = remove_extensions(title)
	title = remove_brackets(title)
	title = replace_underscores(title)
	title = remove_lyrics(title)
	title = remove_featuring(title)
	title = replace_ampersand(title)
	title = remove_punctuation_and_whitespaces(title)

	return title.strip()






