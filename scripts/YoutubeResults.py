#!/usr/bin/python2.6
#encoding: utf-8

from config import NPKeys

import json
import re
import requests
import operator

import logging
logging.basicConfig(level=logging.INFO, format='[%(levelname)s] (%(threadName)-10s) %(message)s',)
logging.getLogger("requests").setLevel(logging.WARNING)

import SongExtractor as se

class YoutubeResults(se.SongExtractor):

    def __init__(self):
        super( se.SongExtractor, self ).__init__()

        self.url = "https://www.googleapis.com/youtube/v3/search"
        self.regex = re.compile('(.+?)-(.+)', re.IGNORECASE)
        self.keys = NPKeys()
    

    def extract(self, tweet, test=False):
        tweetText = self.clean(tweet['text'])
        yt_results = self.query_yt(tweetText)

        if tweetText == "" or len(tweetText) < 3:
            return None

        results = []
        if yt_results and "pageInfo" in yt_results and yt_results["pageInfo"]["totalResults"] > 0:
            for result in yt_results["items"]:
                results.append( result["snippet"]["title"] )
        else:
            return None

        song_candidates = {}
        artist_candidates = {}
        both_candidates = {}

        result_counter = 11
        seen_pink = False
        pink_pattern1 = re.compile(r'p!nk', re.IGNORECASE|re.UNICODE)
        pink_pattern2 = re.compile(r'pink', re.IGNORECASE|re.UNICODE)

        for result in results:
            result_counter -= 1
            result = self.process_text(result)

            m = self.regex.match(result)
            if m == None:
                continue

            artist = se.tp.dash_names_decode(m.group(1))
            song = se.tp.dash_names_decode(m.group(2))

            if not seen_pink and (pink_pattern1.search(artist) or pink_pattern1.search(song)):
                seen_pink = True
                should_delete = []
                new_artist_candidates = {}
                new_song_candidates = {}
                new_both_candidates = {}

                for a in artist_candidates:
                    new_a = pink_pattern2.sub(r'p!nk', a)
                    new_artist_candidates[new_a] = artist_candidates[a]
                    should_delete.append(a)
                
                for d in should_delete:
                    del artist_candidates[d]
                artist_candidates.update(new_artist_candidates)

                should_delete = []
                for s in song_candidates:
                    new_s = pink_pattern2.sub(r'p!nk', s)
                    new_song_candidates[new_s] = song_candidates[s]
                    should_delete.append(s)

                for d in should_delete:
                    del song_candidates[d]
                song_candidates.update(new_song_candidates)

                should_delete = []
                for b in both_candidates:
                    new_b = pink_pattern2.sub(r'p!nk', b)
                    new_both_candidates[new_b] = both_candidates[b]
                    should_delete.append(b)

                for d in should_delete:
                    del both_candidates[d]
                both_candidates.update(new_both_candidates)


            if seen_pink:
                artist = pink_pattern2.sub(r'p!nk', artist)
                song = pink_pattern2.sub(r'p!nk', song)

            if artist in artist_candidates:
                artist_candidates[artist]+=result_counter
            else:
                artist_candidates[artist]=result_counter

            if song in song_candidates:
                song_candidates[song]+=result_counter
            else:
                song_candidates[song]=result_counter

            if artist in both_candidates:
                both_candidates[artist]+=result_counter
            else:
                both_candidates[artist]=result_counter
            if song in both_candidates:
                both_candidates[song]+=result_counter
            else:
                both_candidates[song]=result_counter

        artists = sorted(artist_candidates.iteritems(), key=operator.itemgetter(1), reverse=True)
        songs   = sorted(song_candidates.iteritems(), key=operator.itemgetter(1), reverse=True)
        both = sorted(both_candidates.iteritems(), key=operator.itemgetter(1), reverse=True)

        seen_artists = 0
        seen_songs = 0

        for b in range(len(both)):
            if b == len(both)-1:
                break

            item = both[b]
            old_value = 0

            for a in range(b+1, len(both)):
                if both[a][1] < old_value:
                    no_ties = True
                    break

                next_item = both[a]
                old_value = both[a][1]

                if item[1] < 4 or next_item[1] < 4:
                    break

                check = se.mb.exact_match(item[0], next_item[0])
                if check is None:
                    continue
                else:
                    return check

        # Try fuzzy match
        if len(artists)>0 and len(songs)>0:
            check = se.mb.fuzzy_match(artists[0][0], songs[0][0])
            if check:
                return check


    def query_yt(self, query):
        key = self.keys.get_youtube_keys(is_random=True)
        params  = {'part':'snippet', 'key':key, 'alt':'json', 'type':'video', 'videoCategoryId':10, 'maxResults':10, 'q':query.encode("utf-8")}

        rjson = None
        try:
            r = requests.get(self.url, params=params, timeout=1)
            r.encoding = 'utf-8'

            return r.json()
        except:
            return None


    def clean(self, s):
        s = se.tp.remove_urls(s)
        s = se.tp.replace_underscores(s)

        rs = re.compile(r"\s+", re.UNICODE | re.MULTILINE)
        rnp = re.compile(r"\b(np|nowplaying|rt|via|retweet(ing)?|listenlive|swtchr)\b", re.UNICODE)
        rp = re.compile(r"(\W+)", re.UNICODE)

        s = s.lower()
        s = rs.sub(" ", s)

        s = rnp.sub(" ", s)
        s = rp.sub(" ", s)
        s = s.strip()

        return s

    
    def process_text(self, title):
        try:
            title = se.tp.remove_extensions(title)
            title = se.tp.remove_brackets(title)
            title = se.tp.replace_underscores(title)
            title = se.tp.remove_lyrics(title)
            title = se.tp.remove_featuring(title)
            title = se.tp.replace_ampersand(title)
            title = se.tp.dash_names_encode(title)
            title = se.tp.remove_punctuation_and_whitespaces(title)
            title = se.tp.remove_urls(title)
            title = se.tp.translate_diacritics(title)
        except:
            raise

        return title.strip()
