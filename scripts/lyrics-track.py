#encoding: utf-8

from config import NPDatabase, NPKeys, NPTracker

import sys
import re
import requests
import datetime
import logging
import time
import collections
from ssl import SSLError

import pymongo
import multiprocessing
import threading
import lockfile
import json

from tweepy.streaming import StreamListener
from tweepy.error import TweepError
from tweepy import OAuthHandler, Cursor
from tweepy import Stream, Status, API


NUMBER_OF_WORKERS = 5 # number of processes to spawn

WAIT_ON_SOFT_ERROR = 5 # seconds
WAIT_ON_HARD_ERROR = 20 # seconds
WAIT_BEFORE_POLL = 30 
WAIT_BETWEEN_POLLS = 4 * 60 # seconds

USERBATCHSIZE = 25 # Add USERBATCHSIZE users to the list per call
USERBATCHWAIT = 0.2 # Wait for USERBATCHWAIT seconds between calls

last_message_timestamp = datetime.datetime.now()
logging.basicConfig(level=logging.DEBUG, format='[%(levelname)s] (%(threadName)-10s) %(message)s',)
requests_log = logging.getLogger("requests")
requests_log.setLevel(logging.WARNING)

class LyricSearcher(multiprocessing.Process):

    def __init__(self, queue, dbnp, keys, tracker):
        super(LyricSearcher, self).__init__()
        self.dbnp = dbnp
        self.queue = queue
        self.keys = keys
        self.tracker = tracker

        
    def run(self):
        while True:
            temp_url = None
            temp_context = None

            queue_item = self.queue.get()
            tweet = queue_item[0]
            user = queue_item[1]
            
            if tweet is None and user is None: # got the sentinel, exit
                break
            
            found_tag = False
            try:
                for tag in tweet.entities["hashtags"]:
                    if tag['text'].lower() in self.tracker.get_keywords():
                        found_tag = True
                        break
            except AttributeError:
                logging.debug(tweet.entities)
                
            if found_tag:
                continue

                
            text = re.sub(r'https?:\/\/.*[\r\n]*', '', tweet.text)
            text = re.sub(r'(#|@).+?(\s|$)', '', text)
            text = re.sub(r'[^a-zA-Z0-9_ ]', r' ', text)
            text = re.sub(r' {2,}', r' ', text)
            text = text.strip()

            if text.lower() in user['song'].lower() or text.lower() in user['artist'].lower():
                continue

            lyrics = self.query_lyrics_api(text)
            if lyrics is not None:
                found_lyric = False
                for s in lyrics:
                    if s['title'].lower() == user['song'].lower() and s['artist']['name'].lower() == user['artist'].lower():
                        found_lyric = True
                        try:
                            temp_context = re.sub(r'</*em>', '', s['context'])
                        except:
                            found_lyric = False
                            break
                        temp_url = s['url']
                        break

                if found_lyric:
                    self.dbnp.lyrics.insert({'tweetid':tweet.id, 'tweet':tweet.text, 'created_at': tweet.created_at, 'artists':user['artists'], 'song':user['song'], 'lyrics':temp_context, 'url':temp_url, 'user' : user['user']})
                    self.dbnp.lyrics_popularity.update({'artists':user['artists'], 'song':user['song']}, {'$inc': {'count':1} }, upsert=True)
                    self.dbnp.lyrics_fragments.update({'artists':user['artists'], 'song':user['song'], 'lyrics':temp_context}, {'$inc':{'count':1}}, upsert=True)
                    logging.debug("FOUND LYRICS!")
                    logging.debug({'tweetid':tweet.id, 'tweet':tweet.text, 'created_at': tweet.created_at, 'artist':user['artist'], 'song':user['song'], 'lyrics':temp_context, 'url':temp_url, 'user' : user['user']})
                    logging.debug("") 


    def query_lyrics_api(self, text):
        if len(text) < 5:
            return None

        url = "http://api.lyricsnmusic.com/songs"
        params = {"api_key":self.keys.get_lyricsnmusic_keys(is_random=True), "lyrics":text.encode("utf-8")}

        rjson = None
        try:
            r = requests.get(url, params=params, timeout=1)
            r.encoding = 'utf-8'
            return r.json()
        except:
            return None

        return None



# input: set of users with recent plays and a stream
def process_user_list( max_id, dbnp ):
    logging.debug("Retrieving user list using max_id=%s" % str(max_id))
    since_id = 0
    users = collections.defaultdict(dict)
    results = dbnp.currently_playing.find( {'isRadio' : False, '_id' : { '$gt' : max_id }} ).sort('$natural', 1)
    for res in results:
        max_id = res['_id']
        since_id = res["tweet_id"]
        users[res['userID']] = { 'user' : res['user'], 'song' : res['song'], 'artist' : res['artist'], 'artists' : res['artists'], 'time' : time.time(), 'userID' : res['userID'] }
    logging.debug("  retrieved %d users" % len(users))
    logging.debug("  set new max_id=%s" % str(max_id))
    
    return since_id, max_id, users
#end of process_user_list()



def timeout_check():
    logging.debug("Timeout thread running")
    while True:
        current_time = datetime.datetime.now()
        # grab the lock
        tlock = threading.Lock()
        # context manager wraps the whole thing here, acquires the lock and gets rid of it after its through
        with tlock:
            elapsed = current_time - last_message_timestamp
            if elapsed > datetime.timedelta(seconds=60):
                logging.debug("Longer than 1 minute since last twitter message, exiting")
                sys.exit()
            else:
                pass
                
        time.sleep(5)
    return
#end of timeout_check()



def destroy_all_lists():
    # Get and destroy our lists:
    logging.debug("Retrieve and destroy all lists")
    deleted = True
    while deleted is True:
        for mylist in api.lists_all():
            if 'follow4lyrics' not in mylist.slug:
                deleted = False
                continue
            logging.debug("  %s (id: %d)" % (mylist.slug, mylist.id))
            try:
                api.destroy_list(list_id=mylist.id)
            except TweepError, e:
                logging.warn("Unable to delete list. The error is: %s" % e)
            else:
                deleted = True
            time.sleep(USERBATCHWAIT)
# end of destroy_all_lists



if __name__ == "__main__":
    connection = pymongo.Connection("")
    dbnp = connection.npdemo
    dbnp.authenticate('','')

    dbnp_local = NPDatabase().get_db()
    keys = NPKeys()
    tracker = NPTracker()
    twitter_keys = keys.get_twitter_lyrics_keys()

    # initiate queue
    stream_queue = multiprocessing.Queue()
    
    # start processing queue
    workers = []
    for i in range(NUMBER_OF_WORKERS):
        workers.append(LyricSearcher(stream_queue, dbnp_local, keys, tracker))
        workers[-1].daemon = True
        workers[-1].start()
    
    # authenticate Twitter
    auth = OAuthHandler(twitter_keys['consumer_key'], twitter_keys['consumer_secret'])
    auth.set_access_token(twitter_keys['access_key'], twitter_keys['access_secret'])
    
    # Get an API handle
    api = API(auth)
    
    # Destroy all lists on initialization
    destroy_all_lists()
    
    # Get users to follow
    res = dbnp.currently_playing.find_one( {'isRadio' : False}, sort=[('$natural', -1)] )
    max_id = res['_id']
    since_id = res['tweet_id']
    
    logging.debug("Waiting for %d secs" % WAIT_BEFORE_POLL)
    time.sleep(WAIT_BEFORE_POLL) # wait for 4 minutes
    
    
    while True:
        next_since_id, max_id, userlist = process_user_list( max_id, dbnp )
        
        if not userlist:
            logging.warn("Empty user list. Sleeping for %d secs before trying again." % WAIT_ON_SOFT_ERROR)
            time.sleep(WAIT_ON_SOFT_ERROR)
            continue
        
        # destroy all lists
        destroy_all_lists()
        
        
        logging.debug("Waiting %d secs before creating the list again." % WAIT_ON_SOFT_ERROR)
        time.sleep(WAIT_ON_SOFT_ERROR)
        
        
        # Create a list
        try:
            mylist = api.create_list(name='follow4lyrics', mode='private')
        except TweepError, e:
            logging.error("Cannot create list. The error is: %s" % e)
            logging.error("Sleeping %d secs before retrying" % WAIT_ON_HARD_ERROR)
            time.sleep(WAIT_ON_HARD_ERROR)
            continue
        else:
            logging.debug("Created list (id: %d)" % mylist.id)
            mylistid = mylist.id
        
        
        # Add users to the list
        # Split the userlist to 50 users batches, and add them to
        # the list.
        # Get the mod
        usernames = userlist.keys()
        mod = len(usernames) % USERBATCHSIZE
        if mod: # Try to add only if there is a modulo
            try:
                api.add_list_members(list_id=mylistid, user_ids=usernames[:mod])
            except TweepError, e:
                logging.error("Cannot add batch of members (modulo). The error is: %s. Ignoring the error and moving on." % e)
        
        
        
        error = False
        for i in xrange(len(userlist)//USERBATCHSIZE):
            time.sleep(USERBATCHWAIT) # First sleep, so we sleep when we come from above
            logging.debug("Adding members: %s" % ",".join(map(str, usernames[mod+USERBATCHSIZE*i:mod+USERBATCHSIZE*(i+1)])))
            try:
                api.add_list_members(list_id=mylistid, user_ids=usernames[mod+USERBATCHSIZE*i:mod+USERBATCHSIZE*(i+1)])
            except TweepError, e:
                # error = True
                logging.error("Cannot add batch of members. The error is: %s. Ignoring the error and moving on." % e)
                # break
        # if error:
        #     logging.error("Sleeping %d secs before retrying" % WAIT_ON_HARD_ERROR)
        #     time.sleep(WAIT_ON_HARD_ERROR)
        #     continue
        
        # Give some time between creating the list and getting the 
        # statuses.
        logging.debug("Waiting %d secs before quering the list." % WAIT_BEFORE_POLL)
        time.sleep(WAIT_BEFORE_POLL)
        
        
        logging.debug("Querying the list from %s to %s" % (since_id, next_since_id))
        # Process the list
        try:
            listtimeline = api.list_timeline(list_id=mylistid, since_id=since_id, include_rts=False, count=2000)
        except TweepError, e:
            logging.error("Cannot get the list timeline. The error is: %s" % e)
            logging.error("Sleeping %d secs before retrying" % WAIT_ON_HARD_ERROR)
            time.sleep(WAIT_ON_HARD_ERROR)
            continue
        else:
            for item in listtimeline:
                stream_queue.put((item, userlist[item.user.id]))
            logging.info("Got back %d tweets" % len(listtimeline))
        since_id = next_since_id
        
        logging.debug("Waiting %d secs before next poll." % WAIT_BETWEEN_POLLS)
        time.sleep(WAIT_BETWEEN_POLLS)
        
    
    # For some reason we exited the loop. Send sentinel to the queue
    # and shutdown the workers gracefully
    for i in xrange(len(workers)):
        stream_queue.put((None, None))
    
    for worker in workers:
        worker.join()
    
    logging.info("Exited gracefully.")
#end of if__main__

