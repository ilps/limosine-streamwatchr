#!/usr/bin/python2.6
#encoding: utf-8

import json
import re
import requests

import logging
logging.getLogger("requests").setLevel(logging.WARNING)

import TextProcessor as tp
import datetime

def exact_match(artist, song, two_ways=True):
    artistTmp = re.sub(r'"',r'\"',artist)
    songTmp = re.sub(r'"',r'\"',song)

    artist_match = get_exact_artists(artistTmp, tp.remove_dash(songTmp))
    if artist_match:
        return artist_match

    if two_ways:
        artist_match = get_exact_artists(songTmp, tp.remove_dash(artistTmp))
        if artist_match:
            return artist_match

    return None


def get_exact_artists(artist, song):
    query = 'recording:"%s" AND artist:"%s"' % ( song.encode("utf-8"), artist.encode("utf-8") )
    result = query_mb("recording", query)
    if result is not None and result["recording-list"]["count"] > 0:
        current_recording = order_recordings(result)

        song = tp.remove_end_brackets(current_recording['title'])
        artists = current_recording['artist-credit']['name-credit']
        mbArtists = []

        for ar in artists:
            name = ar['artist']['name']
            if name == "[unknown]" or name == "Various Artists" or name == "[no artist]":
                return None

            arid = ar['artist']['id']
            mbArtists.append({'name':name,'mbId':arid})

        return {'artists':mbArtists,'song':song}

    return None

def fuzzy_match(artist, song, two_ways=True):
    found_artist = False;

    # try to match artist - song
    artist_match = get_fuzzy_artists(artist,tp.remove_dash(song))
    if artist_match and artist_match['found_song']:
        return {'artists':artist_match['artists'],'song':artist_match['song']}
    elif artist_match:
        found_artist = True
        artistId = artist_match['artistid']
        storedSong = tp.remove_end_brackets(tp.remove_dash(artist_match['song']))

    if two_ways:
        # try to match song - artist
        artist_match = get_fuzzy_artists(song,tp.remove_dash(artist))
        if artist_match and artist_match['found_song']:
            return {'artists':artist_match['artists'],'song':artist_match['song']}
        elif artist_match and not found_artist:
            found_artist = True
            artistId = artist_match['artistid']
            storedSong = tp.remove_end_brackets(tp.remove_dash(artist_match['song']))

    # if we found an artist we guess the song
    if found_artist:
        query = 'arid:"%s"' % ( artistId.encode("utf-8") )
        result = query_mb("artist",query)
        if result is not None and result['artist-list']['count'] > 0:
            artists = [{'name':result['artist-list']['artist'][0]['name'],'mbId':result['artist-list']['artist'][0]['id']}]
            return {'artists':artists,'song':storedSong}

    return None

def get_fuzzy_artists(artist, song):
    artistTmp = re.sub(r'"',r'\"',artist)
    songTmp = re.sub(r'"',r'\"',song)

    query = '"%s"' % ( artistTmp.encode("utf-8") )
    result = query_mb("artist", query)

    mbArtistIds = []

    if result is not None and result['artist-list']['count'] > 0:
        for a in result['artist-list']['artist']:
            mbArtistIds.append(a['id'])

        arids = " OR arid:".join(mbArtistIds)
        query = 'recording:%s AND (arid:%s)' % ( songTmp.encode("utf-8"), arids.encode("utf-8") )

        result = query_mb("recording", query)
        if result is not None and result["recording-list"]["count"] > 0:
            current_recording = order_recordings(result)

            song = tp.remove_end_brackets(current_recording['title'])
            artists = current_recording['artist-credit']['name-credit']
            mbArtists = []

            for ar in artists:
                name = ar['artist']['name']
                if name == "[unknown]" or name == "Various Artists" or name == "[no artist]":
                    return None

                arid = ar['artist']['id']
                mbArtists.append({'name':name,'mbId':arid})

            return {'artists':mbArtists,'song':song,'found_song':True}

        return {'artistid':mbArtistIds[0],'found_song':False,'song':song}

    return None


def query_mb(method, query):
    url = "http://YOURMACHINE:YOURPORT/ws/2/"+method+"/"
    params  = {'query':query, 'fmt':"json"}

    rjson = None
    try:
        r = requests.get(url, params=params, timeout=1)
        r.encoding = 'utf-8'
        rjson = r.json()
    except:
        return None
        
    return rjson

def order_recordings(result):
    current_recording = result["recording-list"]["recording"][0]
    current_release = datetime.date(1900,1,1)

    for recording in result["recording-list"]["recording"]:
        oldest_release = datetime.date.today()
        if int(recording['score']) < 90:
            break

        if "release-list" in recording:
            for release in recording['release-list']['release']:
                if "date" in release:
                    date = oldest_release
                    if len(release['date']) == 4:
                        date = datetime.date(int(release['date']), 1, 1)
                    elif len(release['date']) == 7:
                        date = datetime.datetime.strptime(release['date'], '%Y-%m').date()
                    else:
                        date = datetime.datetime.strptime(release['date'], '%Y-%m-%d').date()

                    if date < oldest_release:
                        oldest_release = date

        if oldest_release > current_release:
            current_release = oldest_release
            current_recording = recording

    return current_recording

