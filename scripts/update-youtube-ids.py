#encoding: utf-8

import sys
import json
import datetime
import requests
import multiprocessing
import random

from config import NPDatabase, NPKeys

# import and set logging
import logging
logging.basicConfig(level=logging.DEBUG, format='[%(levelname)s] (%(processName)s - %(threadName)-10s) %(message)s',)
logging.getLogger("requests").setLevel(logging.WARNING)


class VideoWorker(multiprocessing.Process):

    def __init__(self, queue, dbnp, keys):
        super(VideoWorker, self).__init__()
        
        self.dbnp = dbnp
        self.queue = queue
        self.keys = keys        

        self.now = datetime.datetime.utcnow()
    
    def run(self):
        while True:
            song = self.queue.get()
            if song is None:
                break

            # Store Youtube videoId
            query = '"' + song['artists'][0]['name'] + '" "' + song['song'] + '"'
            videoId = self.get_youtube_video(query)
            if videoId:
                self.dbnp.song_info.update( {"_id": song['_id']}, { "$set": {"videoId":videoId, "date":self.now}, "$inc": {"update_count":1}} )
            else:
                self.dbnp.song_info.remove({"_id": song['_id']})


    def get_youtube_video(self,query):
        key = self.keys.get_youtube_keys(is_random=True)

        params  = {'part':"id", 'key':key, 'alt':'json', 'maxResults':1, 'type':'video', 'videoCategoryId':10, 'videoEmbeddable':'true', 'q':query.encode("utf-8")}
        try:
            search_response = requests.get("https://www.googleapis.com/youtube/v3/search", params=params, timeout=1)
            search_response.encoding = 'utf-8'
            json_object = search_response.json()

            if json_object["pageInfo"]["totalResults"] > 0:
               return json_object["items"][0]["id"]["videoId"]

            return None
        except:
            return None

        return None


if __name__ == "__main__":
    """ Connect to MongoDB and set up database """
    dbnp = NPDatabase().get_db()
    keys = NPKeys()
    
    # initiate queue
    queue = multiprocessing.Queue()
    jobs = []

    # start processing queue
    for i in range(5):
        t1 = VideoWorker(queue,dbnp, keys)
        jobs.append(t1)
        jobs[-1].daemon = True
        jobs[-1].start()

    # fill queue
    now = datetime.datetime.utcnow()
    lastweek = now-datetime.timedelta(days=7)

    numsongs = dbnp.song_info.find({'date':{'$lt':lastweek}, 'update_count':{'$lt':5} }).count();
    logging.debug("Queuing %d songs from %s" % (numsongs, lastweek))
    for song in dbnp.song_info.find({'date':{'$lt':lastweek}, 'update_count':{'$lt':5} }):
        queue.put(song)

    for i in range(5):
        queue.put(None)

    for job in jobs:
        job.join()
