#!/usr/bin/python2.6
#encoding: utf-8

import json
import re

import SongExtractor as se

class BaselineRegex(se.SongExtractor):

    def __init__(self):
        super( se.SongExtractor, self ).__init__()

        # compile regexes for baseline method
        self.baselineRegexes = []
        #   song_title by artist
        self.baselineRegexes.append( {'artist':2, 'song':1, 'pattern':re.compile('.*?#np:? (.+) by (.+)', re.IGNORECASE)} )
        self.baselineRegexes.append( {'artist':2, 'song':1, 'pattern':re.compile('(.+) by (.+?) #np', re.IGNORECASE)} )
        self.baselineRegexes.append( {'artist':2, 'song':1, 'pattern':re.compile('.*?#nowplaying:? (.+) by (.+)', re.IGNORECASE)} )
        self.baselineRegexes.append( {'artist':2, 'song':1, 'pattern':re.compile('(.+) by (.+?) #nowplaying', re.IGNORECASE)} )
        #   artist - song_title
        self.baselineRegexes.append( {'artist':1, 'song':2, 'pattern':re.compile('.*?#np:? (.+?) - (.+)', re.IGNORECASE)} )
        self.baselineRegexes.append( {'artist':1, 'song':2, 'pattern':re.compile('(.+?) - (.+?) #np', re.IGNORECASE)} )
        self.baselineRegexes.append( {'artist':1, 'song':2, 'pattern':re.compile('.*?#nowplaying:? (.+?) - (.+)', re.IGNORECASE)} )
        self.baselineRegexes.append( {'artist':1, 'song':2, 'pattern':re.compile('(.+?) - (.+?) #nowplaying', re.IGNORECASE)} )
        #   #artist
        self.baselineRegexes.append( {'artist':2, 'song':1, 'pattern':re.compile('.*?#np:? (.+?) #(.+?) ', re.IGNORECASE)} )
        self.baselineRegexes.append( {'artist':1, 'song':2, 'pattern':re.compile('.*?#(.+?) (.+?) #np', re.IGNORECASE)} )
        self.baselineRegexes.append( {'artist':1, 'song':2, 'pattern':re.compile('.*?#np:? #(.+?) (.+)', re.IGNORECASE)} )
        self.baselineRegexes.append( {'artist':2, 'song':1, 'pattern':re.compile('(.+?) #(.+?) #np', re.IGNORECASE)} )
        self.baselineRegexes.append( {'artist':2, 'song':1, 'pattern':re.compile('.*?#nowplaying:? (.+?) #(.+?) ', re.IGNORECASE)} )
        self.baselineRegexes.append( {'artist':1, 'song':2, 'pattern':re.compile('.*?#(.+?) (.+?) #nowplaying', re.IGNORECASE)} )
        self.baselineRegexes.append( {'artist':1, 'song':2, 'pattern':re.compile('.*?#nowplaying:? #(.+?) (.+)', re.IGNORECASE)} )
        self.baselineRegexes.append( {'artist':2, 'song':1, 'pattern':re.compile('(.+?) #(.+?) #nowplaying', re.IGNORECASE)} )


    def extract(self, tweet):
        tweetText = se.tp.remove_hashwords(
                        se.tp.dash_names_encode(
                            se.tp.translate_diacritics(
                                se.tp.remove_urls(
                                    tweet['text'] ) ) ) )

        for regex in self.baselineRegexes:
            m = regex['pattern'].match(tweetText)
            if m == None:
                continue

            artist = self.process_text( m.group(regex['artist']))
            song = self.process_text( m.group(regex['song']))

            if len(artist) < 1:
                continue

            # check in MusicBrainz
            check = se.mb.exact_match(artist,song)
            if check is None:
                continue

            return check

        return None

    def process_text(self, title):
        title = se.tp.dash_names_decode(title.strip())
        title = se.tp.remove_extensions(title)
        title = se.tp.remove_brackets(title)
        title = se.tp.replace_underscores(title)
        title = se.tp.remove_lyrics(title)
        title = se.tp.remove_featuring(title)
        title = se.tp.replace_ampersand(title)
        title = se.tp.remove_punctuation_and_whitespaces(title)
    
        return title.strip()
