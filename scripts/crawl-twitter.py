#encoding: utf-8

from config import NPKeys, NPTracker

import sys
import datetime
import logging
import time
from ssl import SSLError

import threading
import lockfile
import json
import pymongo

from tweepy.streaming import StreamListener
from tweepy import OAuthHandler
from tweepy import Stream, Status


last_message_timestamp = datetime.datetime.now()
logging.basicConfig(level=logging.DEBUG, format='[%(levelname)s] (%(threadName)-10s) %(message)s',)
requests_log = logging.getLogger("requests")
requests_log.setLevel(logging.WARNING)


class StdOutListener(StreamListener):
    
    # initiate stream listener
    def settings( self, db ):
        self.errorCount = 0
        self.db = db
            
    
    def on_data( self, data ):
        try:
            tlock = threading.Lock()
            with tlock:
                global last_message_timestamp
                last_message_timestamp = datetime.datetime.now()

                tweet = json.loads(data)

                # Parse dates in tweet JSON
                tweet['created_at'] = datetime.datetime.strptime(tweet['created_at'],'%a %b %d %H:%M:%S +0000 %Y')
                tweet['user']['created_at'] = datetime.datetime.strptime(tweet['user']['created_at'],'%a %b %d %H:%M:%S +0000 %Y')
                if 'retweeted_status' in tweet:
                    tweet['retweeted_status']['created_at'] = datetime.datetime.strptime(tweet['retweeted_status']['created_at'],'%a %b %d %H:%M:%S +0000 %Y')
                    tweet['retweeted_status']['user']['created_at'] = datetime.datetime.strptime(tweet['retweeted_status']['user']['created_at'],'%a %b %d %H:%M:%S +0000 %Y')

                # Ignore deleted tweets and hitting the rate limit
                if 'delete' in tweet:
                    return False
                elif 'limit' in tweet:
                    tweet['limit']['track']
                    return False
                
                # Finally, store the tweet object in the crawler capped collection
                self.db.crawler.insert( tweet )
                return True

        except Exception as e:
            # log the exception so we can spot problems if they come up in the future
            logging.debug("Exception during stream parsing: %s" % e.__str__())
            logging.debug("Tweet object: %s" % data)
    

    def on_error( self, status ):
        logging.debug("Error returned by twitter: %s" % str(status) )
        self.errorCount += 1
        
        if self.errorCount > 1000: return False
        return
        
        
    def on_timeout( self ):
        # If no post received for too long
        logging.debug("Stream exiting at %s" % str( datetime.datetime.now()) )
        sys.exit()
        return
        
        
    def on_limit( self, track ):
        # If too many posts match our filter criteria and only a subset is sent
        logging.debug("Limit notification returned by twitter: %s" % str(track) )
        return
        
        
    def on_delete( self, status_id, user_id ):
        # When a delete notice arrives for a post.
        return



def timeout_check():
    """ Background thread loop that does nothing but check the last
    message timestamp, if its been more than 1 minutes it means the
    twitter heartbeat signal (or a tweet) hasnt arrived recently which
    means the connection probably dropped for any of 10+ legitimate
    reasons that arent errors but must be handled by exiting and
    letting supervisord restart this process. """
    logging.debug("Timeout thread running")
    while True:
        current_time = datetime.datetime.now()
        # grab the lock
        tlock = threading.Lock()
        # context manager wraps the whole thing here, acquires the lock and gets rid of it after its through
        with tlock:
            elapsed = current_time - last_message_timestamp
            if elapsed > datetime.timedelta(seconds=60):
                logging.debug("Longer than 1 minute since last twitter message, exiting")
                sys.exit()
            else:
                pass
                
        time.sleep(5)
    return
#end of timeout_check()



if __name__ == "__main__":

    connection = pymongo.Connection("")
    dbnp = connection.npdemo
    dbnp.authenticate('','')

    keys = NPKeys()
    twitter_keys = keys.get_twitter_keys()
    tracker = NPTracker()

    # create stream listener
    l = StdOutListener()
    l.settings(dbnp)
    
    # authenticate Twitter
    auth = OAuthHandler(twitter_keys['consumer_key'], twitter_keys['consumer_secret'])
    auth.set_access_token(twitter_keys['access_key'], twitter_keys['access_secret'])
    
    # start streaming
    stream = Stream(auth, l, timeout=30)	
    logging.debug("Starting request stream at %s" % ( str(datetime.datetime.now()) ) )
    
    while True:
        try:
            stream.filter(track=tracker.get_hashtags())
        except Exception:
            logging.exception('stream filter')
            time.sleep(10)
        except SSLError:
            logging.exception('SSL exception')
            time.sleep(2)
    
        
#end of if__main__

