import pymongo as pm
import time

if __name__ == "__main__":
	""" Connect to MongoDB and set up database """

	connection = pm.Connection("")
	dbnp = connection.npdemo
	dbnp.authenticate("", "") # readonly account: demo, npd3m0

	print "Start inserting records"
	counter = 0
	
	for date in dbnp.song_stats_geo_hourly.distinct('date'):
		if dbnp.country_stats_hourly.find_one({'date':date}):
			continue

		r = dbnp.song_stats_geo_hourly.aggregate( [{'$match': { 'date' : date}}, { '$group' : {'_id' : { 'country':"$country", 'isRadio':"$isRadio"}, 'plays': { '$sum' : "$count" } } } ])
		for res in r['result']:
			dbnp.country_stats_hourly.insert({'country':res['_id']['country'], 'date':date, 'isRadio':res['_id']['isRadio'], 'count':res['plays']})
			counter += 1

		print "Inserted %d records" % counter
		print
